﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleUtilities.ConsoleInfoPanel;
using ConsoleUtilities.ConsoleProgressBar;

namespace ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleInformationPanelTests.TestInfoPanelLargeNumberOfProgressbarsAndInfoItems();
            //ConsoleInformationPanelTests.TestInfoPanelLargeNumberOfProgressbars();
            //ConsoleInformationPanelTests.TestInfoPanel();
        }
    }
}
