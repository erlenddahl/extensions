﻿namespace ConsoleUtilities
{
    public interface IRunnable
    {
        void Run();
    }
}