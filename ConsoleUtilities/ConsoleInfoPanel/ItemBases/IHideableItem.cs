﻿namespace ConsoleUtilities.ConsoleInfoPanel.ItemBases
{
    public interface IHideableItem
    {
        bool CanBeHidden { get; }
    }
}