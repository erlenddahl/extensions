﻿namespace ConsoleUtilities.ConsoleCommand
{
    public enum ConsoleCommandParameterType
    {
        String,
        Integer,
        IntegerArray,
        Double,
        Boolean
    }
}