﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LeafletHelpers
{
    public class TripListWithMapGenerator
    {
        private readonly int _zoomPadding;

        private readonly string _baseHtml = @"<!DOCTYPE html>
<html>
<head>
  
  <title>{title}</title>

  <meta charset='utf-8' />
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  
    <link rel='stylesheet' href='https://unpkg.com/leaflet@1.6.0/dist/leaflet.css' integrity='sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==' crossorigin=''/>
    <script src='https://unpkg.com/leaflet@1.6.0/dist/leaflet.js' integrity='sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==' crossorigin=''></script>

    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
    <script src = 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
    <script src = 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/d3/5.15.0/d3.min.js' integrity='sha256-m0QmIsBXcOMiETRmpT3qg2IQ/i0qazJA2miCHzOmS1Y=' crossorigin='anonymous'></script>

    <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' integrity='sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN' crossorigin='anonymous'>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js' integrity='sha256-VeNaFBVDhoX3H+gJ37DpT/nTuZTdjYro9yBruHjVmoQ=' crossorigin='anonymous'></script>

    <style>
      
      #map, body, html{
        width: 100%;
        height: 100%;
      }

      .fh{
        height: 100%;
      }

      .trips-table th{
        cursor: pointer;
      }

      #summary{
        overflow: auto;
      }

      .summary-item {
        margin: 5px;
        width: 300px;
        float: left;
        white-space: nowrap;
      }

      .summary-item .header {
        width: 200px;
        font-weight: bold;
        display: inline-block;
      }

      .summary-item .value {
        width: 100px;
        display: inline-block;
      }

    </style>

</head>
<body>
  <div class='container-fluid fh'>
    <div class='row fh'>
      <div class='col-6 fh' style='overflow: auto;'>
        <h2 id='summary-title'>{summaryHeader}</h2>
        <div id='summary'></div>
        <h2 style='margin-top: 10px;'>{tableHeader}</h2>
        <div id='showPredicates'></div>
        <table id='table' class='table trips-table'>
          <thead><tr></tr></thead>
          <tbody></tbody>
        </table>
      </div>
      <div class='col-6 fh'>
        <div id='map'></div>
      </div>
    </div>
  </div>

  <script>

    var osmLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 20,
      attribution: 'Map data &copy; <a href=\'https://www.openstreetmap.org/\'>OpenStreetMap</a> contributors, ' +
        '<a href=\'https://creativecommons.org/licenses/by-sa/2.0/\'>CC-BY-SA</a>, ' +
        'Imagery © <a href=\'https://www.mapbox.com/\'>Mapbox</a>',
      id: 'mapbox/streets-v11'
    });

    var map = L.map('map', { layers: [osmLayer] });

    var baseMaps = {
        'OpenStreetMap': osmLayer
    };

    var summary = {summary};

    var headers = {headers};

    var showPredicates = {showPredicates};

    var trips = [
      {data}
    ];

    function showAll(predicate, limit){
        var totalBounds = L.latLngBounds();
        var count = 0;
        for(var i = 0; i < trips.length; i++){
            var show = true;
            if(predicate && !predicate(trips[i])) show = false;
            if(limit >= 0 && count>= limit) show = false;
            var line = trips[i];

            if(show) count++;

            if(show && !line.isShowing){
                showLine(line);
                totalBounds.extend(line.polyline.getBounds());
            }else if(!show && line.isShowing){
                line.polyline.remove();
                line.isShowing = false;
            }
        }

        if(count > 0)
            map.fitBounds(totalBounds);
        refreshTable();
    }

    function getTooltip(line){
        var s = '';
        for(var key in line.d)
            s += '<strong>' + key + ': </strong>' + line.d[key] + '<br />';
        return s;
    }

    function showLine(line){
        if(!line.polyline){
            line.polyline = L.polyline(line.p, { color: line.s.color || '#ff0000', weight: line.s.weight || 3, opacity: line.s.opacity || 1.0 });
            line.polyline.bindTooltip(getTooltip(line));

            line.polyline.on('mouseover', function() {
                setActive(this);
            });

            line.polyline.on('mouseout', function() {
                setInactive(this);
            });
        }
        line.polyline.addTo(map);
        line.isShowing = true;
    }

    function setActive(polyline, color){
        polyline.originalStyle = JSON.parse(JSON.stringify(polyline.options));
        polyline.setStyle({
            color: color || '#00ff00',
            weight: 10
        });
        polyline.bringToFront();
        polyline.isActive = true;

        for(var i in trips)
          if(trips[i].polyline && trips[i].polyline.isActive && trips[i].polyline != polyline)
            setInactive(trips[i].polyline);
    }

    function setInactive(polyline){
        polyline.setStyle(polyline.originalStyle);
        polyline.isActive = false;
    }

    d3.select('#showPredicates')
        .selectAll('a')
        .data(showPredicates)
        .enter()
        .append('a')
        .attr('style', 'display: inline-block; margin-right: 7px;')
        .attr('href', '#')
        .text(function(d){ return d.title; })
        .on('click', function(d){ 
            d3.event.preventDefault();
            showAll(d.func, d.limit); 
        });

    function format(value, f){
        if(!f) return value;
        if(f[0] == 'n') return value.toFixed(f[1]);
        return value;
    }

    function createSummary(){
      var cells = d3.select('#summary')
        .selectAll('div.summary-item')
        .data(summary)
        .enter()
          .append('div')
          .attr('class', 'summary-item');

      cells
        .append('div')
        .attr('class', 'header')
        .html(function(d){ return d.title + ': '; });

      cells
        .append('div')
        .attr('class', 'value')
        .html(function(d){ return format(d.value, d.format) + (d.unit ? ' ' + d.unit : ''); });

      if(!summary || !summary.length){
        d3.select('#summary-title').style('display', 'none');
        d3.select('#summary').style('display', 'none');
      }
}

    createSummary();

    function tripCellDataFunc(d, i) { 
      var vals = headers.map(function(p){ return d.d[p.key] }); 
      vals.unshift(''); 
      return vals;  
    }

    function tripCellHtmlFunc(d, i) {
      if(i == 0){
        return '<i class=\'fa fa-' + (d3.select(this.parentNode).datum().isShowing ? 'check-' : '') + 'square-o\' style=\'cursor: pointer;\'></i>'
      }
      return format(d, allHeaders[i].format);
    }

    function tripCellClickFunc(d, i){
      d = d3.select(this.parentNode).datum()
      if(i > 0){
        if(d.isShowing){
          map.fitBounds(d.polyline.getBounds());
          d.polyline.addTo(map);
        }
        return; 
      }
      toggleLineDisplay(d);
      d3.select(this).html('<i class=\'fa fa-' + (d.isShowing ? 'check-' : '') + 'square-o\' style=\'cursor: pointer;\'></i>');
    }

    function refreshTable(){
        d3.select('#table tbody')
        .selectAll('tr')
            .data(trips)
            .selectAll('td')
            .data(tripCellDataFunc)
            .html(tripCellHtmlFunc)
            .on('click', tripCellClickFunc);
    }

    var lastSort = { key: null, dir: true };
    function sortTripsBy(key){
      var dir = lastSort.dir;
      if(lastSort.key == key)  dir = !dir;
      trips = _.orderBy(trips, [function(p){ return p.d[key]; }], [dir ? 'asc' : 'desc']);

      refreshTable();

      lastSort = { key: key, dir: dir };
      return lastSort;
    }

    L.control.layers(baseMaps).addTo(map);

    allHeaders = [ { key: 'isShowing', title: '' } ];
    for(var i = 0; i < headers.length; i++)
      allHeaders.push(headers[i]);

    d3.select('#table thead tr')
      .selectAll('th')
      .data(allHeaders)
      .enter()
      .append('th')
      .text(function(d){ return d.title; })
      .on('click', function(d){
        var sorted = sortTripsBy(d.key);

        d3.select('#table thead tr')
        .selectAll('th')
        .data(allHeaders)
        .html(function(d){ return (sorted.key == d.key ? '<i class=\'fa fa-caret-' + (sorted.dir ? 'up': 'down') + '\'></i> ' : '') + d.title; });

      });

    var tr = d3.select('#table tbody')
        .selectAll('tr')
        .data(trips)
        .enter()
        .append('tr')
        .on('mouseenter', function(d){
          if(!d.isShowing) return;
          setActive(d.polyline);
        })
        .on('mouseleave', function(d){
          if(!d.isShowing) return;
          setInactive(d.polyline);
        });

    var td = tr.selectAll('td')
        .data(tripCellDataFunc)
        .enter()
          .append('td')
          .html(tripCellHtmlFunc)
          .on('click', tripCellClickFunc);

    function toggleLineDisplay(line){
        if(line.isShowing){
          line.isShowing = false;
          line.polyline.remove();
          return;
        }

        if(!line.polyline)
            line.polyline = L.polyline(line.p, { color: line.s.color || '#ff0000', weight: line.s.weight || 3, opacity: line.s.opacity || 1.0 });
        line.polyline.addTo(map);
        line.isShowing = true;

        map.fitBounds(line.polyline.getBounds());
    }

    if(showPredicates && showPredicates[0])
        showAll(showPredicates[0].func, showPredicates[0].limit);

  </script>

</body>
</html>";

        private readonly List<LeafletPolyline> _lines = new List<LeafletPolyline>();
        private readonly List<JObject> _headers = new List<JObject>();
        private readonly List<JObject> _summary = new List<JObject>();
        private readonly List<JObject> _showPredicates = new List<JObject>();

        public string SummaryHeader { get; set; } = "Summary";
        public string TableHeader { get; set; } = "Trips";

        public TripListWithMapGenerator(int zoomPadding = 20)
        {
            _zoomPadding = zoomPadding;
        }

        public LeafletPolyline AddPolyLine<T>(IList<T> points, Func<T, double> getLat, Func<T, double> getLon)
        {
            var trip = new LeafletPolyline(this, points.Select(p=>(getLat(p), getLon(p))).ToArray());
            _lines.Add(trip);
            return trip;
        }

        public string GenerateHtml(string title = "Untitled")
        {
            var sb = new StringBuilder();
            for (var i = 0; i < _lines.Count; i++)
            {
                _lines[i].AppendTo(sb);
                if (i < _lines.Count - 1) sb.AppendLine(",");
            }

            return _baseHtml
                .Replace("{title}", title)
                .Replace("{zoomPadding}", _zoomPadding.ToString())
                .Replace("{data}", sb.ToString())
                .Replace("{summaryHeader}", SummaryHeader)
                .Replace("{tableHeader}", TableHeader)
                .Replace("{headers}", JArray.FromObject(_headers).ToString(Formatting.None))
                .Replace("{summary}", JArray.FromObject(_summary).ToString(Formatting.None))
                .Replace("{showPredicates}", JArray.FromObject(_showPredicates).ToString(Formatting.None));
        }

        public TripListWithMapGenerator AddSummary(string title, object value, string format = null, string unit = null)
        {
            _summary.Add(JObject.FromObject(new {title, value, format, unit}));
            return this;
        }

        public TripListWithMapGenerator AddShowPredicate(string title, string jsFilter, int limit = -1)
        {
            _showPredicates.Add(JObject.FromObject(new {title, func = new JRaw(jsFilter), limit}));
            return this;
        }

        public string OpenTemporary(string title = "Untitled")
        {
            var path= SaveTo(System.IO.Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".html"), title);
            new Process {StartInfo = new ProcessStartInfo(path) {UseShellExecute = true}}.Start();
            return path;
        }

        public string SaveTo(string path, string title = "Untitled")
        {
            var s = GenerateHtml(title);
            System.IO.File.WriteAllText(path, s);
            return path;
        }

        public void EnsureHeaderSequence(string key, string format)
        {
            if (_headers.Any(p => p.Value<string>("key") == key)) return;
            _headers.Add(JObject.FromObject(new {key, title = key, format}));
        }
    }

    public class LeafletPolyline
    {
        private readonly TripListWithMapGenerator _owner;
        private readonly (double lat, double lon)[] _coordinates;
        private readonly JObject _data;
        private readonly JObject _styles = new JObject();
        private JObject _pointData;

        public LeafletPolyline(TripListWithMapGenerator owner, (double lat, double lon)[] coordinates, Dictionary<string, object> data = null)
        {
            _owner = owner;
            _coordinates = coordinates;
            _data = data == null ? new JObject() : new JObject(data);
        }

        public LeafletPolyline Data(string key, object value, string format = null)
        {
            _data[key] = JToken.FromObject(value);
            _owner.EnsureHeaderSequence(key, format);
            return this;
        }

        public LeafletPolyline SetPointData(string key, IEnumerable<JToken> data)
        {
            if (_pointData == null)
                _pointData = new JObject();

            _pointData[key] = new JArray(data);

            return this;
        }

        public LeafletPolyline Color(Color color)
        {
            _styles["color"] = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
            return this;
        }

        public LeafletPolyline Color(string color)
        {
            _styles["color"] = color;
            return this;
        }

        public LeafletPolyline Weight(double value)
        {
            _styles["weight"] = value;
            return this;
        }

        public LeafletPolyline Opacity(double value)
        {
            _styles["opacity"] = value;
            return this;
        }

        public void AppendTo(StringBuilder sb)
        {
            sb.AppendFormat("{{ p: [{0}], s: {1}, d: {2}, pd: {3} }}",
                string.Join(", ", _coordinates.Select(p => "[" + p.lat.ToString("n5", CultureInfo.InvariantCulture) + ", " + p.lon.ToString("n5", CultureInfo.InvariantCulture) + "]")),
                _styles.ToString(Formatting.None),
                _data.ToString(Formatting.None),
                _pointData != null ? _pointData.ToString(Formatting.None) : "null");
        }
    }
}
