﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace LeafletHelpers
{
    public class PureMapGenerator
    {
        private readonly int _zoomPadding;

        private string _baseHtml = @"<!DOCTYPE html>
<html>
<head>
  
  <title>{title}</title>

  <meta charset='utf-8' />
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  
    <link rel='stylesheet' href='https://unpkg.com/leaflet@1.6.0/dist/leaflet.css'/>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.6.0/leaflet-src.js'></script>

    <style>
      
      #map, body, html{
        width: 100%;
        height: 100%;
      }

    </style>

</head>
<body>

  <div id='map'></div>

  <script>

    var osmLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 20,
      attribution: 'Map data &copy; <a href=\'https://www.openstreetmap.org/\'>OpenStreetMap</a> contributors, ' +
        '<a href=\'https://creativecommons.org/licenses/by-sa/2.0/\'>CC-BY-SA</a>, ' +
        'Imagery © <a href=\'https://www.mapbox.com/\'>Mapbox</a>',
      id: 'mapbox/streets-v11'
    });

    var map = L.map('map', { layers: [osmLayer] });

    var baseMaps = {
        'OpenStreetMap': osmLayer
    };

    var layers = {};
    var totalBounds = L.latLngBounds();

    var useLayers = {useLayers};

    {data}

    L.control.layers(baseMaps, layers).addTo(map);
    
    map.fitBounds(totalBounds, { padding: L.point({zoomPadding}, {zoomPadding}) });

    function addPointList(title, points, color){
        var markers = points.map(function(p){ var m = L.circleMarker([p.la, p.ln], { radius: 1, color: p.c || color, fill: false, opacity: p.o || 1.0 }); if(p.i) m.bindTooltip(p.i); return m; });
        var group = L.featureGroup(markers).addTo(map);

        if(useLayers)
            layers[title + ' (' + points.length + ')'] = group;
      
        totalBounds.extend(points.map(function(p){ return [p.la, p.ln]; }));
    }

    function addPoint(title, point){
        var m = L.circleMarker([p.la, p.ln], { radius: p.r || 1, color: p.c || '#ff0000', fill: p.f || false, opacity: p.o || 1.0 }); 
        if(p.i) 
            m.bindTooltip(p.i);
        m.addTo(map);
      
        totalBounds.extend([p.la, p.ln]);
    }

    function addPolyLine(title, points, color, weight, opacity){
        var line = L.polyline(points, { color: color, weight: weight || 3, opacity: opacity || 1.0 });
        line.addTo(map);

        if(useLayers)
            layers[title + ' (' + points.length + ')'] = line;
      
        totalBounds.extend(points);
    }

  </script>

</body>
</html>";

        private StringBuilder _data = new StringBuilder();

        public bool UseLayers { get; set; } = false;

        public PureMapGenerator(int zoomPadding = 20)
        {
            _zoomPadding = zoomPadding;
        }

        /// <summary>
        /// Use extras c for color, r for radius, i for tooltip, f for fill, o for opacity, fc for fill color, fo for fill opacity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="title"></param>
        /// <param name="points"></param>
        /// <param name="getLat"></param>
        /// <param name="getLon"></param>
        /// <param name="defaultColor"></param>
        /// <param name="extras"></param>
        public void AddPointList<T>(string title, IList<T> points, Func<T, double> getLat, Func<T, double> getLon, string defaultColor = "#0000ff", params (string Key, Func<T, string> Func)[] extras)
        {
            var js = "addPointList('" + title + "', [" + string.Join(",", points.Select(p => "{la:" + getLat(p).ToString(CultureInfo.InvariantCulture) + ",ln:" + getLon(p).ToString(CultureInfo.InvariantCulture) + (string.Join("", extras.Select(e => "," + e.Key + ":" + e.Func.Invoke(p)))) + "}")) + "], '" + defaultColor + "');";
            _data.AppendLine(js);
        }

        /// <summary>
        /// Use extras c for color, r for radius, i for tooltip, f for fill, o for opacity, fc for fill color, fo for fill opacity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="title"></param>
        /// <param name="points"></param>
        /// <param name="getLat"></param>
        /// <param name="getLon"></param>
        /// <param name="defaultColor"></param>
        public void AddPointList<T>(string title, IList<T> points, Func<T, double> getLat, Func<T, double> getLon, string defaultColor = "#0000ff")
        {
            var js = "addPointList('" + title + "', [" + string.Join(",", points.Select(p => "{la:" + getLat(p).ToString(CultureInfo.InvariantCulture) + ",ln:" + getLon(p).ToString(CultureInfo.InvariantCulture) + "}")) + "], '" + defaultColor + "');";
            _data.AppendLine(js);
        }

        public void AddPoint(string title, double lat, double lon, string color = "#ff0000", double radius = 1, bool fill = true, double opacity = 1.0)
        {
            var js = "addPoint('" + title + "', {la:" + lat.ToString(CultureInfo.InvariantCulture) + ",ln:" + lon.ToString(CultureInfo.InvariantCulture) + ",c:'" + color + "',r:" + radius + ",f:" + fill + ",o:" + opacity.ToString(CultureInfo.InvariantCulture) + "});";
            _data.AppendLine(js);
        }

        public void AddPolyLine<T>(string title, IList<T> points, Func<T, double> getLat, Func<T, double> getLon, string color = null, double weight = 1, double opacity = 1.0)
        {
            var js = "addPolyLine('" + title + "', [" + string.Join(",", points.Select(p => "[" + getLat(p).ToString(CultureInfo.InvariantCulture) + "," + getLon(p).ToString(CultureInfo.InvariantCulture) + "]")) + "], '" + color + "', " + weight.ToString(CultureInfo.InvariantCulture) + ", " + opacity.ToString(CultureInfo.InvariantCulture) + ");";
            _data.AppendLine(js);
        }

        public string GenerateHtml(string title = "Untitled")
        {
            return _baseHtml
                .Replace("{title}", title)
                .Replace("{useLayers}", UseLayers ? "true" : "false")
                .Replace("{zoomPadding}", _zoomPadding.ToString())
                .Replace("{data}", _data.ToString());
        }

        public string OpenTemporary(string title = "Untitled")
        {
            var s = GenerateHtml(title);
            var path = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".html");
            System.IO.File.WriteAllText(path, s);
            new Process { StartInfo = new ProcessStartInfo(path) { UseShellExecute = true } }.Start();
            return path;
        }

    }
}
